package vn.tcbs.tool.stoxservice.service;

public interface IPartnerService {
	public Long getLastValue(String partner, String key);
	public void updateLastId(String partner, String key, Long lastId);
}
