package vn.tcbs.tool.stoxservice.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the stx_tcbs_Partner database table.
 * 
 */
@Entity
@Table(name="stx_tcbs_Partner")
@NamedQuery(name="TcbsPartner.findAll", query="SELECT t FROM TcbsPartner t")
public class TcbsPartner implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String keyCode;

	private Long keyValue;

	private String partner;

	public TcbsPartner() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getKeyCode() {
		return this.keyCode;
	}

	public void setKeyCode(String keyCode) {
		this.keyCode = keyCode;
	}

	public Long getKeyValue() {
		return this.keyValue;
	}

	public void setKeyValue(Long keyValue) {
		this.keyValue = keyValue;
	}

	public String getPartner() {
		return this.partner;
	}

	public void setPartner(String partner) {
		this.partner = partner;
	}

}