package vn.tcbs.tool.stoxservice.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.Type;


/**
 * The persistent class for the Messages database table.
 * 
 */
@Entity
@Table(name="Messages")
@NamedQuery(name="Message.findAll", query="SELECT m FROM Message m")
public class Message implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="MessageID")
	private Long messageID;

	@Column(name="AuthorFull")
	private String authorFull;

	@Column(name="Authority")
	private String authority;

	@Column(name="BusinessCheckNews")
	private Integer businessCheckNews;

	@Column(name="CategoryID")
	private Integer categoryID;

	private Integer chuyendeID;

	@Column(name="ClickOnPostDate")
	private Integer clickOnPostDate;

	@Column(name="CompanyID")
	private Integer companyID;

	@Column(name="ExpertID")
	private Long expertID;

	@Column(name="IndustryID")
	private Integer industryID;

	@Column(name="IsAnalysis",nullable=true)
	private Boolean isAnalysis;

	@Column(name="IsAudio")
	private Boolean isAudio;

	@Column(name="IsCutOff")
	private Boolean isCutOff;

	private Boolean isExchangeNews;

	@Column(name="IsFeedback")
	private Boolean isFeedback;

	@Column(name="IsHome")
	private Boolean isHome;

	private Boolean isHomeShow;

	@Column(name="IsImportant")
	private Boolean isImportant;

	@Column(name="IsLinkTicker")
	private Boolean isLinkTicker;

	@Column(name="IsMarketOne")
	private Boolean isMarketOne;

	@Column(name="IsMarketThree")
	private Boolean isMarketThree;

	@Column(name="IsMarketTwo")
	private Boolean isMarketTwo;

	@Column(name="IsSalient")
	private Boolean isSalient;

	@Column(name="IsSalientB")
	private Boolean isSalientB;

	@Column(name="IsSendToMember")
	private Boolean isSendToMember;

	@Column(name="IsSolution")
	private Boolean isSolution;

	@Column(name="IsSpecial")
	private Boolean isSpecial;

	@Column(name="IsStockHot")
	private Boolean isStockHot;

	@Column(name="IsTop")
	private Boolean isTop;

	@Column(name="IsTransfer")
	private Boolean isTransfer;

	@Column(name="LastModify")
	private String lastModify;

	@Column(name="MsgAlign")
	private Integer msgAlign;

	@Column(name="MsgAuthor")
	private String msgAuthor;

	@Column(name="MsgContent")
	private String msgContent;

	@Column(name="MsgHistory")
	private String msgHistory;

	@Column(name="MsgHome")
	private String msgHome;

	@Column(name="MsgHomeImage")
	private String msgHomeImage;

	@Lob
	@Column(name="MsgHomeImage_Byte")
	private byte[] msgHomeImage_Byte;

	@Column(name="MsgImageComment")
	private String msgImageComment;

	@Column(name="MsgKeyword")
	private String msgKeyword;

	@Column(name="MsgLead")
	private String msgLead;

	@Column(name="MsgLinkSource")
	private String msgLinkSource;

	@Column(name="MsgOrder")
	private Integer msgOrder;

	@Column(name="MsgPostDate")
	private Date msgPostDate;

	@Column(name="MsgSmallImage")
	private String msgSmallImage;

	@Lob
	@Column(name="MsgSmallImage_Byte")
	private byte[] msgSmallImage_Byte;

	@Column(name="MsgStatus")
	private Integer msgStatus;

	@Column(name="MsgSubTitle")
	private String msgSubTitle;

	@Column(name="MsgTitle")
	private String msgTitle;

	@Column(name="MsgType")
	private String msgType;

	@Column(name="Poster")
	private String poster;

	@Column(name="RedirectLink")
	private String redirectLink;

	@Column(name="ReferenceID")
	private Integer referenceID;

	@Column(name="Royalties")
	private Double royalties;

	@Column(name="StoxID")
	private Long stoxID;

	@Column(name="SubTitle")
	private String subTitle;

	@Column(name="Ticker")
	private String ticker;

	@Column(name="TotalClick")
	private Integer totalClick;

	public Message() {
	}

	public Long getMessageID() {
		return this.messageID;
	}

	public void setMessageID(Long messageID) {
		this.messageID = messageID;
	}

	public String getAuthorFull() {
		return this.authorFull;
	}

	public void setAuthorFull(String authorFull) {
		this.authorFull = authorFull;
	}

	public String getAuthority() {
		return this.authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public Integer getBusinessCheckNews() {
		return this.businessCheckNews;
	}

	public void setBusinessCheckNews(Integer businessCheckNews) {
		this.businessCheckNews = businessCheckNews;
	}

	public Integer getCategoryID() {
		return this.categoryID;
	}

	public void setCategoryID(Integer categoryID) {
		this.categoryID = categoryID;
	}

	public Integer getChuyendeID() {
		return this.chuyendeID;
	}

	public void setChuyendeID(Integer chuyendeID) {
		this.chuyendeID = chuyendeID;
	}

	public Integer getClickOnPostDate() {
		return this.clickOnPostDate;
	}

	public void setClickOnPostDate(Integer clickOnPostDate) {
		this.clickOnPostDate = clickOnPostDate;
	}

	public Integer getCompanyID() {
		return this.companyID;
	}

	public void setCompanyID(Integer companyID) {
		this.companyID = companyID;
	}

	public Long getExpertID() {
		return this.expertID;
	}

	public void setExpertID(Long expertID) {
		this.expertID = expertID;
	}

	public Integer getIndustryID() {
		return this.industryID;
	}

	public void setIndustryID(Integer industryID) {
		this.industryID = industryID;
	}

	public Boolean getIsAnalysis() {
		return this.isAnalysis;
	}

	public void setIsAnalysis(Boolean isAnalysis) {
		this.isAnalysis = isAnalysis;
	}

	public Boolean getIsAudio() {
		return this.isAudio;
	}

	public void setIsAudio(Boolean isAudio) {
		this.isAudio = isAudio;
	}

	public Boolean getIsCutOff() {
		return this.isCutOff;
	}

	public void setIsCutOff(Boolean isCutOff) {
		this.isCutOff = isCutOff;
	}

	public Boolean getIsExchangeNews() {
		return this.isExchangeNews;
	}

	public void setIsExchangeNews(Boolean isExchangeNews) {
		this.isExchangeNews = isExchangeNews;
	}

	public Boolean getIsFeedback() {
		return this.isFeedback;
	}

	public void setIsFeedback(Boolean isFeedback) {
		this.isFeedback = isFeedback;
	}

	public Boolean getIsHome() {
		return this.isHome;
	}

	public void setIsHome(Boolean isHome) {
		this.isHome = isHome;
	}

	public Boolean getIsHomeShow() {
		return this.isHomeShow;
	}

	public void setIsHomeShow(Boolean isHomeShow) {
		this.isHomeShow = isHomeShow;
	}

	public Boolean getIsImportant() {
		return this.isImportant;
	}

	public void setIsImportant(Boolean isImportant) {
		this.isImportant = isImportant;
	}

	public Boolean getIsLinkTicker() {
		return this.isLinkTicker;
	}

	public void setIsLinkTicker(Boolean isLinkTicker) {
		this.isLinkTicker = isLinkTicker;
	}

	public Boolean getIsMarketOne() {
		return this.isMarketOne;
	}

	public void setIsMarketOne(Boolean isMarketOne) {
		this.isMarketOne = isMarketOne;
	}

	public Boolean getIsMarketThree() {
		return this.isMarketThree;
	}

	public void setIsMarketThree(Boolean isMarketThree) {
		this.isMarketThree = isMarketThree;
	}

	public Boolean getIsMarketTwo() {
		return this.isMarketTwo;
	}

	public void setIsMarketTwo(Boolean isMarketTwo) {
		this.isMarketTwo = isMarketTwo;
	}

	public Boolean getIsSalient() {
		return this.isSalient;
	}

	public void setIsSalient(Boolean isSalient) {
		this.isSalient = isSalient;
	}

	public Boolean getIsSalientB() {
		return this.isSalientB;
	}

	public void setIsSalientB(Boolean isSalientB) {
		this.isSalientB = isSalientB;
	}

	public Boolean getIsSendToMember() {
		return this.isSendToMember;
	}

	public void setIsSendToMember(Boolean isSendToMember) {
		this.isSendToMember = isSendToMember;
	}

	public Boolean getIsSolution() {
		return this.isSolution;
	}

	public void setIsSolution(Boolean isSolution) {
		this.isSolution = isSolution;
	}

	public Boolean getIsSpecial() {
		return this.isSpecial;
	}

	public void setIsSpecial(Boolean isSpecial) {
		this.isSpecial = isSpecial;
	}

	public Boolean getIsStockHot() {
		return this.isStockHot;
	}

	public void setIsStockHot(Boolean isStockHot) {
		this.isStockHot = isStockHot;
	}

	public Boolean getIsTop() {
		return this.isTop;
	}

	public void setIsTop(Boolean isTop) {
		this.isTop = isTop;
	}

	public Boolean getIsTransfer() {
		return this.isTransfer;
	}

	public void setIsTransfer(Boolean isTransfer) {
		this.isTransfer = isTransfer;
	}

	public String getLastModify() {
		return this.lastModify;
	}

	public void setLastModify(String lastModify) {
		this.lastModify = lastModify;
	}

	public Integer getMsgAlign() {
		return this.msgAlign;
	}

	public void setMsgAlign(Integer msgAlign) {
		this.msgAlign = msgAlign;
	}

	public String getMsgAuthor() {
		return this.msgAuthor;
	}

	public void setMsgAuthor(String msgAuthor) {
		this.msgAuthor = msgAuthor;
	}

	public String getMsgContent() {
		return this.msgContent;
	}

	public void setMsgContent(String msgContent) {
		this.msgContent = msgContent;
	}

	public String getMsgHistory() {
		return this.msgHistory;
	}

	public void setMsgHistory(String msgHistory) {
		this.msgHistory = msgHistory;
	}

	public String getMsgHome() {
		return this.msgHome;
	}

	public void setMsgHome(String msgHome) {
		this.msgHome = msgHome;
	}

	public String getMsgHomeImage() {
		return this.msgHomeImage;
	}

	public void setMsgHomeImage(String msgHomeImage) {
		this.msgHomeImage = msgHomeImage;
	}

	public byte[] getMsgHomeImage_Byte() {
		return this.msgHomeImage_Byte;
	}

	public void setMsgHomeImage_Byte(byte[] msgHomeImage_Byte) {
		this.msgHomeImage_Byte = msgHomeImage_Byte;
	}

	public String getMsgImageComment() {
		return this.msgImageComment;
	}

	public void setMsgImageComment(String msgImageComment) {
		this.msgImageComment = msgImageComment;
	}

	public String getMsgKeyword() {
		return this.msgKeyword;
	}

	public void setMsgKeyword(String msgKeyword) {
		this.msgKeyword = msgKeyword;
	}

	public String getMsgLead() {
		return this.msgLead;
	}

	public void setMsgLead(String msgLead) {
		this.msgLead = msgLead;
	}

	public String getMsgLinkSource() {
		return this.msgLinkSource;
	}

	public void setMsgLinkSource(String msgLinkSource) {
		this.msgLinkSource = msgLinkSource;
	}

	public Integer getMsgOrder() {
		return this.msgOrder;
	}

	public void setMsgOrder(Integer msgOrder) {
		this.msgOrder = msgOrder;
	}

	public Date getMsgPostDate() {
		return this.msgPostDate;
	}

	public void setMsgPostDate(Date msgPostDate) {
		this.msgPostDate = msgPostDate;
	}

	public String getMsgSmallImage() {
		return this.msgSmallImage;
	}

	public void setMsgSmallImage(String msgSmallImage) {
		this.msgSmallImage = msgSmallImage;
	}

	public byte[] getMsgSmallImage_Byte() {
		return this.msgSmallImage_Byte;
	}

	public void setMsgSmallImage_Byte(byte[] msgSmallImage_Byte) {
		this.msgSmallImage_Byte = msgSmallImage_Byte;
	}

	public Integer getMsgStatus() {
		return this.msgStatus;
	}

	public void setMsgStatus(Integer msgStatus) {
		this.msgStatus = msgStatus;
	}

	public String getMsgSubTitle() {
		return this.msgSubTitle;
	}

	public void setMsgSubTitle(String msgSubTitle) {
		this.msgSubTitle = msgSubTitle;
	}

	public String getMsgTitle() {
		return this.msgTitle;
	}

	public void setMsgTitle(String msgTitle) {
		this.msgTitle = msgTitle;
	}

	public String getMsgType() {
		return this.msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getPoster() {
		return this.poster;
	}

	public void setPoster(String poster) {
		this.poster = poster;
	}

	public String getRedirectLink() {
		return this.redirectLink;
	}

	public void setRedirectLink(String redirectLink) {
		this.redirectLink = redirectLink;
	}

	public Integer getReferenceID() {
		return this.referenceID;
	}

	public void setReferenceID(Integer referenceID) {
		this.referenceID = referenceID;
	}

	public Double getRoyalties() {
		return this.royalties;
	}

	public void setRoyalties(Double royalties) {
		this.royalties = royalties;
	}

	public Long getStoxID() {
		return this.stoxID;
	}

	public void setStoxID(Long stoxID) {
		this.stoxID = stoxID;
	}

	public String getSubTitle() {
		return this.subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getTicker() {
		return this.ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

	public Integer getTotalClick() {
		return this.totalClick;
	}

	public void setTotalClick(Integer totalClick) {
		this.totalClick = totalClick;
	}

}