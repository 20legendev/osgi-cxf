package vn.tcbs.tool.stoxservice.dao;

import java.util.List;

import vn.tcbs.tool.stoxservice.entity.Company;

public interface CompanyDAO {
	public List<Company> listCompany(String search);
}