package vn.tcbs.tool.stoxservice.dao;

import vn.tcbs.tool.stoxservice.entity.TcbsPartner;

public interface IPartnerDAO {
	public TcbsPartner getByPartner(String partner, String key);
	public void update(String partner, String key, Long lastId);
}
