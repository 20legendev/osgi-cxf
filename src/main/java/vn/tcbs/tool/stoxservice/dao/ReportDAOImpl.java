package vn.tcbs.tool.stoxservice.dao;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.springframework.stereotype.Repository;

import vn.tcbs.tool.base.BaseDAO;
import vn.tcbs.tool.stoxservice.entity.DividendYield;

@Repository("reportDAO")
@Transactional
public class ReportDAOImpl extends BaseDAO implements ReportDAO {

	private static final Logger logger = Logger.getLogger(ReportDAOImpl.class);

	@Override
	public List<Map<String, Object>> getByCompany(String code, int year, int quarter) {
		// TODO Auto-generated method stub
		Query q = getSession()
				.createSQLQuery(
						"EXEC Tcbs_FscData_FinancialReport_Get @Code = :Code, @FYear = :FYear, @Quarter = :Quarter")
				.setParameter("Code", code).setParameter("FYear", year)
				.setParameter("Quarter", quarter);
		q.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		List<Map<String, Object>> list = q.list();
		return list;
	}

	@Override
	public List<DividendYield> getCashDiv(String code, int year) {
		Query q = getSession().createQuery("FROM DividendYield WHERE yearReport = ? and ticker = ?")
				.setParameter(0, Double.valueOf(year)) 
				.setParameter(1, code);
		return q.list();
	}
}