package vn.tcbs.tool.stoxservice.dao;

import java.util.List;

import vn.tcbs.tool.stoxservice.entity.Category;

public interface ICategoryDAO {
	public List<Category> listCategory();
}