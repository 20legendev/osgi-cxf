package vn.tcbs.tool.stoxservice.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.springframework.stereotype.Repository;

import vn.tcbs.tool.base.BaseDAO;
import vn.tcbs.tool.stoxservice.entity.Company;

@Repository("companyDAO")
@Transactional
public class CompanyDAOImpl extends BaseDAO implements CompanyDAO {

	private static final Logger logger = Logger.getLogger(CompanyDAOImpl.class);

	@Override
	public List<Company> listCompany(String search) {
		Query q = null;
		String select = "id, ticker, shortName,registedOffice, districtID, cityID";
		if(search != ""){
			q = getSession().createQuery("SELECT " + select + " FROM Company");
		}else{
			q = getSession().createQuery("SELECT " + select + " FROM Company WHERE shortName like :likeString' ").setString("likeString", "%" + search + "%");
		}
		q.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		return q.list();
	}
}