package vn.tcbs.tool.stoxservice.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import vn.tcbs.tool.base.BaseDAO;
import vn.tcbs.tool.stoxservice.entity.Message;
import vn.tcbs.tool.stoxservice.entity.TcbsPartner;

@Repository("partnerDAO")
@Transactional
public class PartnerDAO extends BaseDAO implements IPartnerDAO {

	@Override
	public TcbsPartner getByPartner(String partner, String key) {
		Query q = getSession()
				.createQuery(
						"from TcbsPartner where partner = :partner AND keyCode = :keyCode")
				.setParameter("partner", partner).setParameter("keyCode", key);
		// q.setResultTransformer(Transformers.aliasToBean(TcbsPartner.class));
		List<TcbsPartner> result = q.list();
		if (result.size() > 0) {
			return result.get(0);
		} else {
			return null;
		}
	}

	@Override
	public void update(String partner, String key, Long lastId) {
		TcbsPartner p = this.getByPartner(partner, key);
		if (p == null) {
			p = new TcbsPartner();
			p.setPartner(partner);
			p.setKeyCode(key);
			p.setKeyValue(lastId);
			getSession().save(p);
		} else {
			p.setKeyValue(lastId);
			getSession().update(p);
		}

	}

}
