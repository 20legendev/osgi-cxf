package vn.tcbs.tool.stoxservice.dao;

import java.util.List;
import java.util.Map;

import vn.tcbs.tool.stoxservice.entity.DividendYield;

public interface ReportDAO {
	public List<Map<String, Object>> getByCompany(String code, int year, int quarter);
	public List<DividendYield> getCashDiv(String code, int year);
}