package vn.tcbs.tool.stoxservice.dao;

import java.util.Date;
import java.util.List;

import vn.tcbs.tool.stoxservice.entity.Message;

public interface MessageDAO {
	public List<Message> getMessage(Integer page, Integer perpage,
			Integer categoryId, String ticker, String language, Date date);

	public Message getMessageById(Long messageId);

	public Long countMessage(Integer categoryId, String ticker,
			String language, Date date);
}