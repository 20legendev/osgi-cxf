package vn.tcbs.tool.stoxservice.dao;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.springframework.stereotype.Repository;

import vn.tcbs.tool.base.BaseDAO;
import vn.tcbs.tool.stoxservice.entity.Message;
import vn.tcbs.tool.stoxservice.entity.Messages_EN;

@Repository("messageDAO")
@Transactional
public class MessageDAOImpl extends BaseDAO implements MessageDAO {

	private static final Logger logger = Logger.getLogger(MessageDAOImpl.class);

	@Override
	public List<Message> getMessage(Integer page, Integer perpage,
			Integer categoryId, String ticker, String language, Date date) {

		Criteria cr = this.getQuery(categoryId, ticker, language, date);
		cr.addOrder(Order.desc("msgPostDate"));
		cr.setMaxResults(perpage).setFirstResult(page * perpage);
		List<Message> res = cr.list();
		return res;
	}

	@Override
	public Long countMessage(Integer categoryId, String ticker,
			String language, Date date) {
		Criteria cr = getSession().createCriteria(
				(language == null || language.equals("") || language
						.equals("vn")) ? Message.class : Messages_EN.class);

		if (categoryId != null) {
			cr.add(Restrictions.eq("categoryID", categoryId));
		} else if (ticker != null) {
			cr.add(Restrictions.eq("ticker", ticker));
		}
		if (date != null) {
			Date endDate = new Date(date.getTime() + (1000 * 60 * 60 * 24));
			cr.add(Restrictions.ge("msgPostDate", date)).add(
					Restrictions.lt("msgPostDate", endDate));
		}
		cr.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		cr.setProjection(Projections.rowCount());
		return (Long) cr.list().get(0);
	}

	@Override
	public Message getMessageById(Long messageId) {
		Message message = (Message) getSession().get(Message.class, messageId);
		return message;
	}

	private Criteria getQuery(Integer categoryId, String ticker,
			String language, Date date) {
		Criteria cr = getSession().createCriteria(
				(language == null || language.equals("") || language
						.equals("vn")) ? Message.class : Messages_EN.class);

		if (categoryId != null) {
			cr.add(Restrictions.eq("categoryID", categoryId));
		} else if (ticker != null) {
			cr.add(Restrictions.eq("ticker", ticker));
		}
		if (date != null) {
			Date endDate = new Date(date.getTime() + (1000 * 60 * 60 * 24));
			cr.add(Restrictions.ge("msgPostDate", date)).add(
					Restrictions.lt("msgPostDate", endDate));
		}
		cr.setProjection(
				Projections
						.projectionList()
						.add(Projections.property("messageID"), "messageID")
						.add(Projections.property("categoryID"), "categoryID")
						.add(Projections.property("msgTitle"), "msgTitle")
						.add(Projections.property("msgLead"), "msgLead")
						.add(Projections.property("msgHomeImage"),
								"msgHomeImage")
						.add(Projections.property("msgPostDate"), "msgPostDate")
						.add(Projections.property("msgAuthor"), "msgAuthor")
						.add(Projections.property("ticker"), "ticker"))
				.setResultTransformer(
						AliasToEntityMapResultTransformer.INSTANCE);
		return cr;
	}

}