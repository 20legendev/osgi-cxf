package vn.tcbs.tool.stoxservice.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;

import vn.tcbs.tool.base.BaseDAO;
import vn.tcbs.tool.stoxservice.entity.Category;

@Repository("companyDAO")
@Transactional
public class CategoryDAO extends BaseDAO implements ICategoryDAO {

	private static final Logger logger = Logger.getLogger(CategoryDAO.class);

	@Override
	public List<Category> listCategory() {
		Criteria q = getSession().createCriteria(Category.class);
		return q.list();
	}
}