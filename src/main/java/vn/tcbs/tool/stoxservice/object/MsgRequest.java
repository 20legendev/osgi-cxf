package vn.tcbs.tool.stoxservice.object;

import java.sql.Timestamp;

public class MsgRequest {
	private Integer page;
	private Integer perpage;
	private Integer category;
	private String ticker;
	private String language;
	private String partner;
	private String bydate;

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getPerpage() {
		return perpage;
	}

	public void setPerpage(Integer perpage) {
		this.perpage = perpage;
	}

	public Integer getCategory() {
		return category;
	}

	public void setCategory(Integer category) {
		this.category = category;
	}

	public String getTicker() {
		return ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getPartner() {
		return partner;
	}

	public void setPartner(String partner) {
		this.partner = partner;
	}

	public String getBydate() {
		return bydate;
	}

	public void setBydate(String bydate) {
		this.bydate = bydate;
	}
}
