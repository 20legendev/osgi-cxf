package vn.tcbs.api.stoxservice;

import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@WebService
@Path("/news")
public class NewsService implements INewsService {

	@GET
	@Path("/test")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON,
			MediaType.APPLICATION_FORM_URLENCODED })
	public Response test() {
		return Response.ok("Hello world")
				.header("Access-Control-Allow-Origin", "*").build();
	}

}
